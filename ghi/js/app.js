function createCard(name, description, pictureUrl, start, end, building) {
    return `
        <div class="card shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${building}</h6>
                <p class="card-text">${description}</p>
            </div>
        </div>
        <div class="card-footer">
            ${start} - ${end}
        </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Incorrect URL")
      } else {
        const data = await response.json();

        let index = 0;
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDay = new Date(details.conference.starts);
                const endDay = new Date(details.conference.ends);
                const building = details.conference.location.name;

                let startDate = startDay.getDate();
                let startMonth = startDay.getMonth();
                let startYear = startDay.getFullYear();

                let endDate = endDay.getDate();
                let endMonth = endDay.getMonth();
                let endYear = endDay.getFullYear();

                let start = `${startMonth}/${startDate}/${startYear}`
                let end = `${endMonth}/${endDate}/${endYear}`

                const html = createCard(title, description, pictureUrl, start, end, building);
                const column = document.querySelector(`#col-${index % 3}`);
                column.innerHTML += html;
                index += 1;
            }
        }

      }
    } catch (e) {
        console.error(e);
    }
});
